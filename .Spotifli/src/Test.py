import json
import os
import configparser
from pathlib import Path



# |> --------------------- <|


class lines():
    def __init__(self, content=[]):
        self.content = content
        
class line():
    def __init__(self, content=[]):
        self.content = content
        
def line_subarray (obj, start:int, length:int):
    """Will return a 2D array based on the passed in objects content

    Args:
        obj (pad): The object to extract line content from
        start (int): The start index
        length (int): The number of elements to include for each index

    Returns:
        list: 2D array of extracted information
    """
    output = []
    
    for line in obj.content:
        output.append(line.content[start:start+length])
    
    return output
        

lines = lines([line(["Hello", "World", "!"]), line(["This", "is", "fun"])])

subarray = subarray(lines, 0, 1)

print(subarray)

