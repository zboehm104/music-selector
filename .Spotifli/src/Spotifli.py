#!/usr/bin/env pypy3
# coding=utf-8

"""The main UI application that handles the application logic."""

import curses
import copy
from curses import KEY_UP, wrapper
import re
import configparser
import time
import os
from pathlib import Path
from threading import Thread, currentThread
from time import sleep

from API import API
from UI import window, line, element


class Spotifli():
    """Main UI and driver class for the application."""

    def __init__(self):
        """Initialize the config and API class."""
        os.chdir(str(Path.home()) + '/.Spotifli/src')
        self.read_config()
        self.check_valid_config()

        self.CLIAPP = API(self.config, self.path)
        self.thread1 = Thread()
        if os.path.isfile(str(Path.home()) + "/.Spotifli/Errors.log") == False:
                open(str(Path.home()) + "/.Spotifli/Errors.log", "x")
        with open(str(Path.home()) + "/.Spotifli/Errors.log", "+w") as Error_Log:
            Error_Log.write("")

    def read_config(self):
        """Find the correct config path then read the config."""
        home = str(Path.home())
        dot_folder = "/.Spotifli"
        config_name = "spotifli.conf"
        config_path = home + dot_folder + "/" + config_name
        config_path_alt = home + "/.config/spotifli/spotifli.conf"
        self.path = config_path
        self.config = configparser.ConfigParser()

        if os.path.isfile(config_path_alt):
            self.config.read(config_path_alt)
            self.path = config_path_alt
        elif os.path.isfile(config_path):
            self.config.read(config_path)
        else:
            print("No config file")
            exit(5)

    # FIXME Temp method
    # TODO Replace with default values if blank
    def check_valid_config(self):
        """Will take the current config and ensure that the keys have values."""
        changed_entries = 0
        for key in self.config['Login'].keys():
            if not self.config['Login'][key]:
                print("Enter the value for " + key + ":")
                self.config['Login'][key] = input()
                changed_entries += 1
        for key in self.config['Player'].keys():
            if not self.config['Player']:
                print("Enter the value for " + key + ":")
                self.config['Player'] = input()
                changed_entries += 1
        # If values where changed then write that to config file
        if changed_entries > 0:
            with open(self.path, "+w") as config_file:
                self.config.write(config_file)

    def player_mode(self, key):
        """Process they key input for player mode."""
        if key in ['q']:
            self.back()
        if key == ' ':
            self.CLIAPP.play_pause()
            if self.CLIAPP.playing:
                self.windows[0].title = "Playing"
            else:
                self.windows[0].title = "Paused"
            self.windows[0].refresh()
        if key == '=':
            self.CLIAPP.vol_up()
            self.windows[0].content[0].content[8] = str(self.CLIAPP.cur_vol)
            self.windows[0].refresh()
        if key == '-':
            self.CLIAPP.vol_down()
            self.windows[0].content[0].content[8] = str(self.CLIAPP.cur_vol)
            self.windows[0].refresh()
        if key == 'n':
            self.CLIAPP.skip_next()
        if key == 'p':
            self.CLIAPP.skip_previous()
        if key == 'm':
            self.CLIAPP.mute()
            self.windows[0].content[0].content[8] = str(self.CLIAPP.cur_vol)
            self.windows[0].refresh()
        if key == 'u':
            self.CLIAPP.toggle_repeat()
            self.windows[0].content[0].content[5] = self.CLIAPP.repeat
            self.windows[0].refresh()
        if key == 's':
            self.CLIAPP.toggle_shuffle()
            shuffle_state = "off"
            if self.CLIAPP.shuffle:
                shuffle_state = "on"
            self.windows[0].content[0].content[2] = shuffle_state
            self.windows[0].refresh()
        if key == 'd':
            self.select_multi_purpose(self.devices(), 'Devices', 'devices')
       

    def window_select_mode(self, key):
        if key == 'h' or key == 'l' or key == 'KEY_LEFT' or key == 'KEY_RIGHT':
            if self.selected_window is not None:
                if self.selected_window == 5:
                    self.change_selected_window(6)
                elif self.selected_window == 6:
                    self.change_selected_window(5)
            else:
                self.change_selected_window(1)
        if key == 'j' or key == 'KEY_DOWN':
            if self.selected_window is not None:
                if self.multi_window:
                    if self.selected_window == 1:
                        self.change_selected_window(3)
                    elif self.selected_window == 3:
                        self.change_selected_window(1)
                else:
                    if self.selected_window == 3:
                        self.change_selected_window(4)
                    elif self.selected_window  == 4:
                        self.change_selected_window(6)
                    elif self.selected_window in [5,6]:
                        self.change_selected_window(3)
            else:
                if self.multi_window:
                    self.change_selected_window(1)
                else:
                    self.change_selected_window(4)
        if key == 'k' or key == 'KEY_UP':
            if self.selected_window is not None:
                if self.multi_window:
                    if self.selected_window in [3,4]:
                        self.change_selected_window(1)
                    elif self.selected_window == 1:
                        self.change_selected_window(3)
                else:
                    if self.selected_window == 3:
                        self.change_selected_window(6)
                    elif self.selected_window in [5,6]:
                        self.change_selected_window(4)
                    elif self.selected_window == 4:
                        self.change_selected_window(3)
            else:
                self.change_selected_window(1)
        if key == '\n':
            if self.selected_window is not None:
                self.change_state(2)

    def change_selected_window(self, index: int):
        self.selected_window = index
        for i, win in enumerate(self.windows):
            if i == index:
                win.refresh(state='focus')
            elif win is not None:
                win.refresh(state='normal')

    def input_mode(self, key):
        """Process key input for search mode."""
        if key == '':
            self.change_selected_window(1)
            self.back()
        if key is not None:
            if re.match('^[a-zA-Z0-9\s]+$', str(key)):
                search = self.windows[2].pad.content + str(key)
                if self.windows[2].get_dim()[0] - 5 > len(search):
                    self.windows[2].refresh(content=search)

        if key == 'KEY_BACKSPACE':
            self.windows[2].refresh(content=self.windows[2].pad.content[:-1])

        if key == '\n':
            self.search(self.windows[2].pad.content.strip())
            self.windows[2].refresh(content="", state="normal")

    def select_multi_purpose(self, new_content: list, new_title: str, key: str):
        self.selected_window = 1
        self.multi_window = True
        self.change_state(2)
        if self.windows[1] is None:
            self.windows[1] = self.build_window(1)
        self.windows[1].refresh(content=new_content, title=new_title, key=key, state='focus')
        self.remove_window([4,5,6])
        self.change_selected_window(1)

    def back(self):
        """Return to previous state or exit application."""
        if self.state == 0:
            self.exit()
        elif self.state != 0:
            self.change_state(0)

    def exit(self):
        """Exit the application."""
        # self.thread1.do_run = False
        self.running = False

    def search(self, search_query: str):
        """Search spotify with the given query."""
        self.remove_window([1])
        self.multi_window = False
        # # 4 5 6 7
        result = self.CLIAPP.get_search_results(search_query)
        self.windows[4] = self.build_window(4)
        self.windows[4].refresh(content=result, key='tracks')
        self.windows[5] = self.build_window(5)
        self.windows[5].refresh(content=result, key='playlists')
        self.windows[6] = self.build_window(6)
        self.windows[6].refresh(content=result, key='albums')
        self.change_selected_window(4)
        self.change_state(0)
        pass

    def devices(self):
        """Select the device to use."""
        return self.CLIAPP.devices_get()

    def selection_mode(self, Key):
        """Controls for selecting an option in the window."""
        if Key is not None:
            if Key == 'h' or Key == 'KEY_LEFT':
                self.windows[self.selected_window].refresh(col=-1)
            if Key == 'j' or Key == 'KEY_UP':
                self.windows[self.selected_window].refresh(row=-1)
            if Key == 'k' or Key == 'KEY_DOWN':
                self.windows[self.selected_window].refresh(row=1)
            if Key == 'l' or Key == 'KEY_RIGHT':
                self.windows[self.selected_window].refresh(col=1)
            if Key == '\n':
                pad = self.windows[self.selected_window].pad
                obj_type, uris, pos = pad.get_data()
                if obj_type is not None:
                    if obj_type == 'device':
                        self.CLIAPP.devices_set(uris)
                        self.windows[0].content[0].content[8] = str(self.CLIAPP.cur_vol)
                        self.windows[0].content[0].content[5] = self.CLIAPP.repeat
                        shuffle_state = "off"
                        if self.CLIAPP.shuffle:
                            shuffle_state = "on"
                        self.windows[0].content[0].content[2] = shuffle_state
                        self.windows[0].refresh()
                        self.change_state(0)
                    if obj_type == 'tracks':
                        self.CLIAPP.play_selected(obj_type=obj_type, uris=uris, position=pos)
                    if obj_type == 'playlist':
                        self.select_multi_purpose(self.CLIAPP.get_playlist_songs(uris), 'Songs', 'tracks')
                    if obj_type == 'album':
                        self.select_multi_purpose(self.CLIAPP.get_album_songs(uris), 'Songs', 'tracks')

    def get_browse_lines(self):
        """Return an array of lines that are the categories for browsing."""
        lines = []
        lines.append(line(["Liked Songs"], data=None))
        lines.append(line(["Discover Weekly"], data=['playlists', '37i9dQZEVXcX1eOARzq390']))
        lines.append(line(["Release Radar"], data=None))
        lines.append(line(["On Repeat"], data=None))
        lines.append(line(["Repeat Rewind"], data=None))
        lines.append(line(["Daily Drive"], data=None))
        return lines

    def init_colors(self):
        """Initialize the colors for curses."""
        if 'Colors' in (self.config.keys()):
            # Primary Text Color
            if 'text' in (self.config['Colors'].keys()):
                RGB = self.config['Colors']['text'].split(',')
                for index in range(0, len(RGB)):
                    RGB[index] = RGB[index].strip()
                if len(RGB) == 3:
                    curses.init_color(curses.COLOR_YELLOW, self.to_rgb(int(RGB[0])), self.to_rgb(int(RGB[1])), self.to_rgb(int(RGB[2])))

            # Normal
            if 'normal' in (self.config['Colors'].keys()):
                RGB = self.config['Colors']['normal'].split(',')
                for index in range(0, len(RGB)):
                    RGB[index] = RGB[index].strip()
                if len(RGB) == 3:
                    curses.init_color(curses.COLOR_BLUE, self.to_rgb(int(RGB[0])), self.to_rgb(int(RGB[1])), self.to_rgb(int(RGB[2])))
            # Focused
            if 'focus' in (self.config['Colors'].keys()):
                RGB = self.config['Colors']['focus'].split(',')
                for index in range(0, len(RGB)):
                    RGB[index] = RGB[index].strip()
                if len(RGB) == 3:
                    curses.init_color(curses.COLOR_MAGENTA, self.to_rgb(int(RGB[0])), self.to_rgb(int(RGB[1])), self.to_rgb(int(RGB[2])))
            # Alert
            if 'alert' in (self.config['Colors'].keys()):
                RGB = self.config['Colors']['alert'].split(',')
                for index in range(0, len(RGB)):
                    RGB[index] = RGB[index].strip()
                if len(RGB) == 3:
                    curses.init_color(curses.COLOR_RED, self.to_rgb(int(RGB[0])), self.to_rgb(int(RGB[1])), self.to_rgb(int(RGB[2])))
            # Background
            if 'background' in (self.config['Colors'].keys()):
                RGB = self.config['Colors']['background'].split(',')
                for index in range(0, len(RGB)):
                    RGB[index] = RGB[index].strip()
                if len(RGB) == 3:
                    curses.init_color(curses.COLOR_BLACK, self.to_rgb(int(RGB[0])), self.to_rgb(int(RGB[1])), self.to_rgb(int(RGB[2])))

        curses.init_pair(1, curses.COLOR_BLUE, curses.COLOR_BLACK),
        curses.init_pair(2, curses.COLOR_MAGENTA, curses.COLOR_BLACK),
        curses.init_pair(3, curses.COLOR_RED, curses.COLOR_BLACK),
        curses.init_pair(4, curses.COLOR_YELLOW, curses.COLOR_BLACK)

        self.colors = {
            "normal": 1,
            "focus": 2,
            "alert": 3,
            "text": 4
        }

    def to_rgb(self, color: int):
        """Take the config 255 based rgb value and converts to cureses 1000 based rgb value."""
        return round((color / 255)*1000)
    
    def build_window(self, type, name: str = "Temp"):
        if type == 4:
            return window(self.colors, dim=[self.Width, (self.Height-17)//2], loc=[0, 7], border=True, title="Tracks")
        elif type == 5:
            return window(self.colors, dim=[self.Width//2, (self.Height-17)//2],loc=[self.Width - self.Width//2, (self.Height-17+7)-((self.Height-17)//2)], border=True, title="Playlists")
        elif type == 6:
            return  window(self.colors, dim=[self.Width//2, (self.Height-17)//2], loc=[0, (self.Height-17+7)-((self.Height-17)//2)], border=True, title="Albums")
        elif type == 1:
            return window(self.colors, dim=[self.Width, self.Height-17], loc=[0, 7], border=True, title=name)
        elif type == 0:
            shuffle_state = "off"
            if self.CLIAPP.shuffle and self.CLIAPP.shuffle is not None:
                shuffle_state = "on"
            if self.CLIAPP.play_state is not None:
                return window(colors=self.colors, dim=[self.Width, 4], loc=[0, 0], border=True, title="Playing", content=[element(loc=[19, 0], content=["( ", "S: ", shuffle_state, ' | ', 'R: ', self.CLIAPP.repeat, ' | ', 'V: ', str(self.CLIAPP.cur_vol), "% )"])])
            else:
                return window(colors=self.colors, dim=[self.Width, 4], loc=[0, 0], border=True, title="Paused", content=[element(loc=[19, 0], content=["( ", "S: ", 'off', ' | ', 'R: ', 'off', ' | ', 'V: ', str(100), "% )"])])
        elif type == 2:
            return window(colors=self.colors, dim=[self.Width-18, 3], loc=[0, 4], border=True, title="Search", highlight=False)
        elif type == 3:
            return window(self.colors, dim=[self.Width, 10], loc=[0, self.Height-10], border= True, title = "Playlists")
        elif type == 7:
            return window(colors=self.colors, dim=[18, 3], loc=[self.Width-18, 4], border=True, title="Mode", highlight=False)

    def remove_window(self, index: list):
        for i in index:
            if self.windows[i] is not None:
                self.windows[i].title = ""
                self.windows[i].refresh(content="",border=False, key='text')
                self.windows[i] = None

    def change_state(self, new_state: int):
        if new_state != self.state:
            self.state = new_state
            if self.state == 0:
                self.windows[7].refresh(content="Window Select")
            elif self.state == 1:
                self.windows[7].refresh(content="Search Spotify")
            elif self.state == 2:
                self.windows[7].refresh(content="Line Select")


    
    def refresh_all(self):
        for index, win in enumerate(self.windows):
            if win is not None:
                if index == 0:
                    new_win = self.build_window(type=0)
                    new_win.pad.content = self.windows[index].pad.content
                    new_win.content = self.windows[index].content
                    new_win.pad.cur_row, new_win.pad.cur_col = self.windows[index].pad.cur_row, self.windows[index].pad.cur_col
                elif index == 1:
                    new_win = self.build_window(type=1, name=self.windows[index].title)
                    new_win.pad.content = self.windows[index].pad.content
                    new_win.pad.cur_row, new_win.pad.cur_col = self.windows[index].pad.cur_row, self.windows[index].pad.cur_col
                elif index == 2:
                    new_win = self.build_window(type=2)
                    new_win.pad.content = self.windows[index].pad.content
                    new_win.pad.cur_row, new_win.pad.cur_col = self.windows[index].pad.cur_row, self.windows[index].pad.cur_col
                elif index == 3:
                    new_win = self.build_window(type=3)
                    new_win.pad.content = self.windows[index].pad.content
                    new_win.pad.cur_row, new_win.pad.cur_col = self.windows[index].pad.cur_row, self.windows[index].pad.cur_col
                elif index == 4:
                    new_win = self.build_window(type=4)
                    new_win.pad.content = self.windows[index].pad.content
                    new_win.pad.cur_row, new_win.pad.cur_col = self.windows[index].pad.cur_row, self.windows[index].pad.cur_col
                elif index == 5:
                    new_win = self.build_window(type=5)
                    new_win.pad.content = self.windows[index].pad.content
                    new_win.pad.cur_row, new_win.pad.cur_col = self.windows[index].pad.cur_row, self.windows[index].pad.cur_col
                elif index == 6: 
                    new_win = self.build_window(type=6)
                    new_win.pad.content = self.windows[index].pad.content
                    new_win.pad.cur_row, new_win.pad.cur_col = self.windows[index].pad.cur_row, self.windows[index].pad.cur_col
                elif index == 7:   
                    new_win = self.build_window(type=6)
                    new_win.pad.content = self.windows[index].pad.content
                    new_win.pad.cur_row, new_win.pad.cur_col = self.windows[index].pad.cur_row, self.windows[index].pad.cur_col
                win.refresh()

    def main(self, stdscr):
        """Call by curses.wrapper to begin the app."""
        # ---Constants & INIT---
        self.Height, self.Width = (curses.LINES, curses.COLS)
        self.p_h, self.p_w = copy.copy(self.Height), copy.copy(self.Width)
        WS = curses.initscr()
        FPS = 1/30
        # Remove blinking curser
        curses.curs_set(0)
        curses.start_color()
        self.init_colors()
        curses.init_pair(1, curses.COLOR_BLUE, curses.COLOR_BLACK)
        # ---QOL Settings---
        WS.nodelay(True)
        self.selected_window = 1
        self.multi_window = True
        
        # >-----< Windows >-----<
        now_playing_window = self.build_window(0)
        playback = self.CLIAPP.get_user_currently_playing(self.Width)
        if playback is not None:
            now_playing_window.refresh(content=self.CLIAPP.get_user_currently_playing(self.Width), key='currently_playing')
        
        track_window = None
        playlist_result_window = None
        album_window = None
        
        results_window = self.build_window(1, 'Devices')
        results_window.refresh(content=self.devices(), key='devices')
        
        search_window = self.build_window(2)
        search_window.refresh(content="", key='text')

        mode_window = self.build_window(7)
        mode_window.refresh(content="Window Select", key="text")

        playlist_window = self.build_window(3)
        playlist_window.refresh(content=self.CLIAPP.get_user_playlists(), key='playlists')

        # >-----< Main Variables >-----<
        self.windows = [now_playing_window, results_window, search_window, playlist_window, track_window, playlist_result_window, album_window, mode_window]
        self.running = True
        self.state = 0
        self.change_state(2)
        self.window = 0
        self.change_selected_window(1)
        cur_playing_update = 0

        # >-----< Main Loop >-----<
        while self.running:
            try:
                key = WS.getkey()
            except:
                key = None

            # Go back or quit
            if self.CLIAPP.selected_device is not None:
                # If not already searching go into search mode
                if self.state != 1 and key == '/':
                    self.change_state(1)
                    self.change_selected_window(2)
                # Process inputs furthur based on the state
                elif self.state == 0:
                    self.player_mode(key)
                    self.window_select_mode(key)
                elif self.state == 1:
                    self.input_mode(key)
                elif self.state == 2:
                    self.player_mode(key)
                    self.selection_mode(key)
            else:
                if key == 'q':
                    self.back()
                self.selection_mode(key)

            # update the currently playing display every 5 seconds
            cur_playing_update += 1
            if cur_playing_update >= 30 * 5:
                playback = self.CLIAPP.get_user_currently_playing(self.Width)
                if playback is not None:
                    self.windows[0].refresh(content=self.CLIAPP.get_user_currently_playing(self.Width))
                cur_playing_update = 0
            time.sleep(FPS)
# >================================================================================<
# >================================================================================<

print("""  
-----------------------------------------------------------
  ██████  ██▓███   ▒█████  ▄▄▄█████▓ ██▓  █████▒██▓        
▒██    ▒ ▓██░  ██▒▒██▒  ██▒▓  ██▒ ▓▒▓██▒▓██   ▒▓██▒    ▓██▒
░ ▓██▄   ▓██░ ██▓▒▒██░  ██▒▒ ▓██░ ▒░▒██▒▒████ ░▒██░    ▒██▒
  ▒   ██▒▒██▄█▓▒ ▒▒██   ██░░ ▓██▓ ░ ░██░░▓█▒  ░▒██░    ░██░
▒██████▒▒▒██▒ ░  ░░ ████▓▒░  ▒██▒ ░ ░██░░▒█░   ░██████▒░██░
▒ ▒▓▒ ▒ ░▒▓▒░ ░  ░░ ▒░▒░▒░   ▒ ░░   ░▓   ▒ ░   ░ ▒░▓  ░░▓  
░ ░▒  ░ ░░▒ ░       ░ ▒ ▒░     ░     ▒ ░ ░     ░ ░ ▒  ░ ▒ ░
░  ░  ░  ░░       ░ ░ ░ ▒    ░       ▒ ░ ░ ░     ░ ░    ▒ ░
      ░               ░ ░            ░             ░  ░ ░  
v1.0
-----------------------------------------------------------

""")
spotifli = Spotifli()
wrapper(spotifli.main)
