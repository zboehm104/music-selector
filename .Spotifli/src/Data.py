import json

from pathlib import Path
from UI import line

class p_search():
    """Object that holds the parsed and formatted information from the api.
    """
    def __init__(self, result):
        self.data = None
        self.parse = {}
        self.widths = {}
        self.parse_result(result)
        
    def convertMillis(self, millis: int):
        """Return formatted string of duration/time.

        Returns:
            str: The duration/time converted from milliseconds
        """
        seconds = (millis//1000) % 60
        minutes = (millis//(1000*60)) % 60
        hours = (millis//(1000*60*60)) % 24

        if hours > 0:
            return str(str(hours) + ':' + str(minutes) + ':' + str(seconds).rjust(2, '0'))
        else:
            return str(str(minutes) + ':' + str(seconds).rjust(2, '0'))
    
    def get_component(self, name):
        """Return the dictionary format of a json template file.

        Args:
            name (str): Name of the component or template file

        Returns:
            dict: The dictionary formatted from the template json file
        """
        file = open(str(Path.home()) + '/.Spotifli/src/Templates/' + name + '.json')
        data = json.load(file)
        file.close()
        return data
    
    def construct_object(self, result, data):
        """Recursive method that constructs a dictionary from templates and keys. Start with the list of items to parse.

        Args:
            result (dict): API dictionary result
            data (dict): The formatted dictionary constructed from the result
        """
        for key in data:
            if key in ['owner', 'playlist', 'track']:
                data[key] = self.get_component(key)
                self.construct_object(result[key], data[key])
            elif key in ['artists', 'devices']:
                for index, item in enumerate(result[key]):
                    data[key].append(self.get_component(key[:-1]))
                    self.construct_object(result[key][index], data[key][index])
            else:
                if key in list(result.keys()):
                    data[key] = result[key]
    
    def parse_result(self, result):
        """Will parse and reduce the information from the passed results. Default for search result.

        Args:
            result (dict): The passed API results

        Returns:
            dict: The reduced information form of the API results
        """
        data = {}
        for key in result:
            data.update({key: []})
            for index, item in enumerate(result[key]['items']):
                data[key].append(self.get_component(key[:-1]))
                self.construct_object(result[key]['items'][index], data[key][index])
        self.data = data
        self.to_lines()
    
    def to_lines(self):
        """Convert formatted api result information into displayable line information.
        """
        # TODO seperate sections into their own functions
        # Construct lines for tracks
        
        self.parse_tracks()
        
        self.parse_playlists()
        
        self.parse_albums()
                
        self.parse_episodes()
                
        self.parse_shows()

        self.parse_devices()
        
        self.line_width()
        self.format_line()
        self.max_size()
    
    def parse_devices(self):
        if 'devices' in self.data.keys() and len(self.data['devices']) > 0:
            self.parse['devices'] = {'header': [], 'lines': [], 'cols': 1, 'widths': [], 'max_width': 0, 'max_height': 0}

            for item in self.data['devices']:
                self.parse['devices']['lines'].append(line(content=[item['name']], data=['device', item['id']]))

    def parse_tracks(self):
        if 'tracks' in self.data.keys() and len(self.data['tracks']) > 0:
            #The lines are in format of [list of lines, start index]
            self.parse['tracks'] = {'header': [], 'lines': [], 'cols': 3, 'widths':[], 'max_width': 0, 'max_height': 0}
            # Add header and a new line
            self.parse['tracks']['header'].append(line(content=['Title','Artist(s)', 'Duration'], data=None))
            self.parse['tracks']['header'].append(line([' ',' ', ' '], data=None))
        
            for item in self.data['tracks']:
                out_line = line(content=[item['name']], data=['track', item['uri']])
                if "artists" in item.keys():
                    if len(item['artists']) > 1:
                        out_line.content.append(str('{' + item['artists'][0]['name'] + ", "+"et al.}"))
                    elif len(item['artists']) == 1: 
                        out_line.content.append(str('{' + item['artists'][0]['name'] + "}"))
                else:
                    out_line.content.append(str("{}"))
                out_line.content.append(self.convertMillis(item['duration_ms']))
                self.parse['tracks']['lines'].append(out_line)
    
    def parse_playlists(self):
        if 'playlists' in self.data.keys() and len(self.data['playlists']) > 0:
            # Generate lines of playlists with name, owner, and number of tracks
            self.parse['playlists'] = {'header': [], 'lines': [], 'cols': 3, 'widths':[], 'max_width': 0, 'max_height': 0}
            self.parse['playlists']['header'].append(line(content=['Title', 'Owner','# Tracks'], data=None))
            self.parse['playlists']['header'].append(line(content=[' ', ' ', ' '], data=None))
            
            for item in self.data['playlists']:
                out_line = line(content=[item['name'],item['owner']['display_name']], data=['playlist', item['uri']])
                out_line.content.append(str(item['tracks']['total']))
                self.parse['playlists']['lines'].append(out_line)
    
    def parse_albums(self):
        if 'albums' in self.data.keys() and len(self.data['albums']) > 0:
            # Generate lines of ablums with name, artists, and number of tracks
            self.parse['albums'] = {'header': [], 'lines': [], 'cols': 3, 'widths':[], 'max_width': 0, 'max_height': 0}
            self.parse['albums']['header'].append(line(content=['Title', 'Artist(s)','# Tracks'], data=None))
            self.parse['albums']['header'].append(line(content=[' ', ' ', ' '], data=None))
            
            for item in self.data['albums']:
                out_line = line(content=[item['name']], data=['album', item['uri']])
                if "artists" in item.keys():
                    if len(item['artists']) > 1:
                        out_line.content.append(str('{' + item['artists'][0]['name'] + ", "+"et al.}"))
                    elif len(item['artists']) == 1: 
                        out_line.content.append(str('{' + item['artists'][0]['name'] + "}"))
                else:
                    out_line.content.append(str("{}"))
                out_line.content.append(str(item['total_tracks']))
                self.parse['albums']['lines'].append(out_line)
    
    def parse_episodes(self):
        if 'episodes' in self.data.keys() and len(self.data['episodes']) > 0:
            # Generate lines of episodes with name, show->publisher, and duration
            self.parse['episodes'] = {'header': [], 'lines': [], 'cols': 2, 'widths':[], 'max_width': 0, 'max_height': 0}
            self.parse['episodes']['header'].append(line(content=['Title','Duration'], data=None))
            self.parse['episodes']['header'].append(line(content=[' ', ' ', ' '], data=None))
            
            for item in self.data['episodes']:
                out_line = line(content=[item['name']], data=['episode', item['uri']])
                out_line.content.append(self.convertMillis(item['duration_ms']))
                self.parse['episodes']['lines'].append(out_line)
    
    def parse_shows(self):
        if 'shows' in self.data.keys() and len(self.data['shows']) > 0:
            # Generate shows with name, publisher, # of episodes
            self.parse['shows'] = {'header': [], 'lines': [], 'cols': 3, 'widths':[], 'max_width': 0, 'max_height': 0}
            self.parse['shows']['header'].append(line(content=['Title', 'Publisher','# Episodes'], data=None))
            self.parse['shows']['header'].append(line(content=[' ', ' ', ' '], data=None))
            
            for item in self.data['shows']:
                out_line = line(content=[item['name'], item['publisher'], str(item['total_episodes'])], data=['show', item['uri']])
                self.parse['shows']['lines'].append(out_line)
    
    def format_line(self):
        """Will return a formatted list of strings created from a list of type line.

        Args:
            lines (list): The list of lines to be formatted
            widths (list): The list of column widths

        Returns:
            list: The list of formatted strings = lines
        """
        for key in self.parse.keys():
            # go through the headers
            for header in self.parse[key]['header']:
                for index, content in enumerate(header.content):
                    if index < self.parse[key]['cols']:
                        header.content[index] = str(content).ljust(self.parse[key]['widths'][index], ' ')
            for cont_lines in self.parse[key]['lines']:
                for index, content in enumerate(cont_lines.content):
                    if index < self.parse[key]['cols']:
                        cont_lines.content[index] = str(content).ljust(self.parse[key]['widths'][index], ' ')

    def line_width(self):
        """Get the width of each colum of the lines.
        """
        for key in self.parse.keys():
            for col in range(0, self.parse[key]['cols']):
                width = 0
                # find max from headers
                for header in self.parse[key]['header']:
                    if len(header.content[col]) > width:
                        width = len(header.content[col])
                # find max from lines
                for cont_line in self.parse[key]['lines']:
                    if len(cont_line.content[col]) > width:
                        width = len(cont_line.content[col])
                # assign to a variable
                self.parse[key]['widths'].append(width)

    def max_size(self):
        for key in self.parse.keys():
            # max width
            width = 0
            for w in self.parse[key]['widths']:
                width += w
            self.parse[key]['max_width'] = width
            # max height
            self.parse[key]['max_height'] = len(self.parse[key]['header']) + len(self.parse[key]['lines'])

    def get_keys_lines(self, key: str):
        out = []
        for item in self.parse[key]['header']:
            out.append(str(item))
        for item in self.parse[key]['lines']:
            out.append(str(item))
        return out
    
    def get_data(self, key: str, index: int):
        if key == 'tracks':
            out = []
            for item in self.parse[key]['lines']:
                out.append(item.data[1])
            return key, out, index - len(self.parse[key]['header']) 
        elif index < len(self.parse[key]['header']):
            return None, None, None
        data = self.parse[key]['lines'][index - len(self.parse[key]['header'])].data
        return data[0], data[1], index - len(self.parse[key]['header']) 
        
# ------------------------------------------------------------------------------------------------   

class p_custom(p_search):
    def __init__(self, text):
        self.data = text
        self.parse = {'text': {'header': [line([' ',' ',' '], None)], 'lines': [], 'cols': 1, 'widths':[], 'max_width': 0, 'max_height': 0}}
        self.widths = {}
        self.parse_result(text)

    def parse_result(self, text):
        """Will parse and reduce the information from the passed results. Default for search result."""
        # Name, artist, progress
        # name
        self.parse['text']['lines'].append(line(content=[text], data=None))
        self.line_width()
        self.max_size()
class p_playlists(p_search):
    def __init__(self, result):
        result = {'playlists': result}
        super().__init__(result)
        
class p_playlist_tracks(p_search):
    def __init__(self, result):
        self.data = None
        self.parse = {}
        self.widths = {}
        result = {'tracks': result}
        self.parse_result(result)
    
    def parse_result(self, result):
        """Will parse and reduce the information from the passed results. Default for search result.

        Args:
            result (dict): The passed API results

        Returns:
            dict: The reduced information form of the API results
        """
        data = {}
        for key in result:
            data.update({key: []})
            for index, item in enumerate(result[key]['items']):
                data[key].append(self.get_component(key[:-1]))
                self.construct_object(result[key]['items'][index]['track'], data[key][index])
        self.data = data
        self.to_lines()

class p_album_tracks(p_search):
    def __init__(self, result):
        result = {'tracks': result}
        super().__init__(result)
    
    def parse_result(self, result):
        """Will parse and reduce the information from the passed results. Default for search result.

        Args:
            result (dict): The passed API results

        Returns:
            dict: The reduced information form of the API results
        """
        data = {}
        for key in result:
            data.update({key: []})
            for index, item in enumerate(result[key]['items']):
                data[key].append(self.get_component(key[:-1]))
                self.construct_object(result[key]['items'][index], data[key][index])

        self.data = data
        self.to_lines()
        
class p_episodes(p_search):
    def __init__(self, result):
        result = {'episodes': result}
        super().__init__(result)
        
class p_devices(p_search):
    def parse_result(self, result):
        """Will parse and reduce the information from the passed results. Default for search result.

        Args:
            result (dict): The passed API results

        Returns:
            dict: The reduced information form of the API results
        """
        data = {}
        for key in result:
            data.update({key: []})
            for index, item in enumerate(result[key]):
                data[key].append(self.get_component(key[:-1]))
                self.construct_object(result[key][index], data[key][index])
        self.data = data
        self.to_lines()
        
# ------------------------------------------------------------------------------------------------  

class p_currently_playing(p_search):
    def __init__(self, result, width: int):
        self.width = width - 4
        super().__init__(result)

    def parse_result(self, result):
        """Will parse and reduce the information from the passed results. Default for search result."""
        # Name, artist, progress
        # name
        self.parse['currently_playing'] = {'header': [], 'lines': [], 'cols': 2, 'widths':[], 'max_width': 0, 'max_height': 0}
        now_playing_line = line([], None)
        if "item" in result.keys() and result['item'] is not None:
            name = result['item']['name']
        else:
            name = 'Unkown'
        if len(name)-1 > self.width:
            name = name[:-5] + '...'
        now_playing_line.content.append(name)
        now_playing_line.content.append("")
        self.parse['currently_playing']['lines'].append(now_playing_line)

        now_playing_line = line([], None)
        if result['item'] is not None:
            if len(result['item']['artists']) > 1:
                now_playing_line.content.append(str("" + result['item']['artists'][0]['name']
                                                + ", " + "et al.").ljust(self.width-10, ' '))
            elif len(result['item']['artists']) > 0:
                now_playing_line.content.append(str(result['item']['artists'][0]['name']).ljust(self.width-10, ' '))
        
            now_playing_line.content.append(self.convertMillis(result['progress_ms']) + '/' + self.convertMillis(result['item']['duration_ms']))
            self.parse['currently_playing']['lines'].append(now_playing_line)
        else:
            now_playing_line.content.append("Unkown")
            now_playing_line.content.append("0:00/0:00")
            self.parse['currently_playing']['lines'].append(now_playing_line)
        self.line_width()
        self.max_size()
# ------------------------------------------------------------------------------------------------  
     
# search = None
# with open(str(Path.home()) + '/Repo/music-selector/Log/SearchResult.log', "r") as read_file:
#     search = p_search(json.load(read_file))
    
# with open(str(Path.home()) + '/Repo/music-selector/Log/Debug.log', '+a') as logger:
#     # logger.write(json.dumps(search.data, indent=2) + '\n\n\n')
#     for key in search.parse.keys():
#         logger.write('\n')
#         out = ''
#         for index, info in enumerate(search.parse[key]['lines']):
#             logger.write(str(info))
#             logger.write(' ||| ' + str(info.data[1]))
#             logger.write('\n')

            
